<?php

const OBDA_WOAR_COPYRIGHT = "Who’s Online Auto Refresher by obda Technologies, © 2024";


/**
 * Add a custom HTML header that triggers a 60-second auto-refresh.
 *
 * If another auto-refresh header already exists, this doesn’t do anything.
 *
 * @param mixed $urls a single url (string) or an array of arrays, each
 *                    inner array being (JSON-encoded request data,
 *                    id_member)
 * @param array $data
 * @return void
 */
function obdaWhosOnlineAutoRefresherAddRefreshHeader(
    &$urls,
    array &$data
): void {
    global $context;
    if (!array_key_exists("html_headers", $context)) {
        $context["html_headers"] = "";
    }
    # Abort if there is already an auto-refresh header present
    $needle_regex = "/<meta\\b[^>]+http-equiv=(['\"]?)refresh\\1/";
    if (preg_match($needle_regex, $context["html_headers"])) {
        return;
    }
    $context["html_headers"] .= '
    <meta http-equiv="refresh" content="60" />';
}


/**
 * Add credits for this modification to the global context.
 *
 * @return void
 */
function obdaWhosOnlineAutoRefresherCredits(): void
{
    global $context;
    $context["copyrights"]["mods"][] = OBDA_WOAR_COPYRIGHT;
}
