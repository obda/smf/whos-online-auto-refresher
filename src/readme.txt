[size=x-large][b]Who’s Online Auto Refresher[/b][/size]

A modification for [url=https://www.simplemachines.org]Simple Machines Forum (SMF) 2.1[/url] that causes the “Who’s Online” view to reload every 60 seconds.

[i]Note: if another modification already added an automatic page refresher to the “Who’s Online” view, this modification will have no effect.[/i]


[size=large][b]Settings[/b][/size]

There are no settings for this mod.  Uninstall it to disable its functionality.


[size=large][b]License[/b][/size]

Who’s Online Auto Refresher is released under the BSD-3-Clause license.


[size=large][b]Changelog[/b][/size]

Version 1.0.1 (2024-05-06):
[list]
[li]Add a prefix to the internal copyright constant’s name to avoid naming collisions.[/li]
[/list]

Version 1.0.0 (2024-05-05):
[list]
[li]First public release.[/li]
[/list]


[size=large][b]Repository[/b][/size]

https://gitlab.com/obda/smf/whos-online-auto-refresher
