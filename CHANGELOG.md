Changelog
=========

Version 1.0.1
-------------

Released 2024-05-06

  * Add a prefix to the internal copyright constant’s name to avoid naming
    collisions.


Version 1.0.0
-------------

Released 2024-05-05

  * First public release.
