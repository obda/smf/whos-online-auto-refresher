<?php

declare(strict_types=1);

namespace obda\WhosOnlineAutoRefresher;

use PHPUnit\Framework\TestCase;

/**
 * Tests for the `obdaWhosOnlineAutoRefresherAddRefreshHeader()` function.
 *
 * @backupGlobals enabled
 */
final class AddRefreshHeaderTest extends TestCase
{
    public function testRefreshHeaderAdded(): void
    {
        global $context;
        $context = [];
        $urls = $data = [];
        obdaWhosOnlineAutoRefresherAddRefreshHeader($urls, $data);
        $meta = '<meta http-equiv="refresh" content="60" />';
        $this->assertStringContainsString($meta, $context["html_headers"]);
    }

    public function testExistingRefreshHeaderRespected(): void
    {
        global $context;
        $context = [
            "html_headers" => '<meta http-equiv="refresh" content="20" />',
        ];
        $urls = $data = [];
        $meta = '<meta http-equiv="refresh" content="60" />';
        obdaWhosOnlineAutoRefresherAddRefreshHeader($urls, $data);
        $this->assertStringNotContainsString($meta, $context["html_headers"]);
    }
}
