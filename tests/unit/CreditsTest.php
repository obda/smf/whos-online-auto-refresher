<?php

declare(strict_types=1);

namespace obda\WhosOnlineAutoRefresher;

use PHPUnit\Framework\TestCase;

/**
 * Tests for the `obdaWhosOnlineAutoRefresherCredits()` function.
 *
 * @backupGlobals enabled
 */
final class CreditsTest extends TestCase
{
    /**
     * Return strings that are expected to show up in the copyright notice.
     *
     * @return array[]
     */
    public static function copyrightSubstringProvider(): array
    {
        return [
            ["Who’s Online Auto Refresher"],  # The modification title
            ["obda Technologies"],
            ["© 2024"],
        ];
    }

    /**
     * Verify that a given substring occurs in the added copyright notice.
     *
     * @param string $substring The string to check for.
     * @return void
     *
     * @dataProvider copyrightSubstringProvider
     */
    public function testCopyrightSubstring(string $substring): void
    {
        global $context;
        $context = [];
        obdaWhosOnlineAutoRefresherCredits();
        $message = $context["copyrights"]["mods"][0];
        $this->assertStringContainsString($substring, $message);
    }
}
