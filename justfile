# List all available recipes
help:
    @just --list

# Run PHPUnit tests
test *args:
    docker run --rm \
    -v $(pwd):/app \
    jitesoft/phpunit:7.3-8 \
    {{ args }}

# Lint PHP code
lint *args=".":
    docker run --rm \
    -v $(pwd):/data cytopia/phpcs:3-php7.3 \
    --standard=PSR12 \
    {{ args }}

# Create the package and put it in the `build` directory
build:
    #!/bin/bash
    export ARCHIVE=obdaWhosOnlineAutoRefresher-$(git describe --abbrev).tar.gz
    mkdir -p build
    cd src
    tar czf ../build/$ARCHIVE *
    echo $ARCHIVE
