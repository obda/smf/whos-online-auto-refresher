Who’s Online Auto Refresher
===========================

A modification for [Simple Machines Forum (SMF) 2.1][1] that causes the
“Who’s Online” view to reload every 60 seconds.

*Note: if another modification already added an automatic page refresher to the
“Who’s Online” view, this modification will have no effect.*

[1]: https://www.simplemachines.org
